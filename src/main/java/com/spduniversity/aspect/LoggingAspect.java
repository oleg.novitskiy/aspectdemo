package com.spduniversity.aspect;

import com.google.gson.Gson;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

@Aspect
public class LoggingAspect {
    private static final Gson GSON = new Gson();

    @Around("execution(* com.spduniversity.service..*(..))")
    public Object logMethodExecution(ProceedingJoinPoint joinPoint) throws Throwable {
        Object[] args = joinPoint.getArgs();
        String methodName = joinPoint.getSignature().getName();

        log(methodName + " - arguments {" + GSON.toJson(args) + "}");

        Object result;
        try {
            result = joinPoint.proceed(args);
        } catch (Throwable t) {
            log(methodName + " - failed with " + t.getMessage());
            throw t;
        }

        log(methodName + " - result {" + GSON.toJson(result) + "}");

        return result;
    }

    private void log(String message) {
        System.out.println(message);
    }

    @Pointcut("execution(* getUserById(..))")
    private void somePointcut() {
        System.out.println("!!!!!!!!!!!!!!!!");
    }

    @After("com.spduniversity.aspect.LoggingAspect.somePointcut()")
    public void catchGetUserByIdMethod() {
        System.out.println("Get User method!!!");
    }
//
//    @Before("execution(* com.spduniversity.service.*.borrowMoney(..))")
//    public void catchBorrowMoney() {
//        System.out.println("Before advise...");
//    }
//
//    @After("execution (* com.spduniversity.service.*.transferMoney(..))")
//    public void catchTransferMoney() {
//        System.out.println("After Transfer...");
//    }
//
//    @Pointcut("within(com.spduniversity.service..*)")
//    public void inServiceLayer() {}
//
//
    @Pointcut("execution(* com.spduniversity.service.*.*(..)) && @annotation(com.spduniversity.contract.UnsafeOperation)")
    public void unsafeOperationsInServiceLayer() {}

    @AfterReturning(
            pointcut="com.spduniversity.aspect.LoggingAspect.unsafeOperationsInServiceLayer()",
            returning="retVal")
    public void fixBorrowedMoney(Object retVal) {
        System.out.println("**** Borrowed money : " + retVal);
    }

}
