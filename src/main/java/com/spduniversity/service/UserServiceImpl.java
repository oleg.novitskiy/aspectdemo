package com.spduniversity.service;

import com.spduniversity.model.User;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service("userService")
public class UserServiceImpl implements UserService {
    private Map<Integer, User> users = new HashMap<>();
    {
        users.put(1, new User("alex", "tuz", 25));
        users.put(2, new User("tom", "hanks", 50));
        users.put(3, new User("bill", "gates", 60));
        users.put(4, new User("jack", "tompson", 75));
    }

    @Override
    public User getUserById(Integer id) {
        System.out.println("Hello from USER");
        return users.get(id);
    }
}
