package com.spduniversity.service;

public interface BankService {
    long transferMoney(long amount);

    long borrowMoney(long amount);
}
