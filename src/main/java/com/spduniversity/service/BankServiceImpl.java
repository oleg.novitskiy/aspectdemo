package com.spduniversity.service;

import com.spduniversity.contract.UnsafeOperation;
import org.springframework.stereotype.Service;

@Service("bankService")
public class BankServiceImpl implements BankService {

    @Override
    @UnsafeOperation
    public long transferMoney(long amount) {
        System.out.println("Doing money transfer for " + amount);
        return amount;
    }

    @Override
    public long borrowMoney(long amount) {
        System.out.println("Borrow money : " + amount);
        return amount;
    }
}
