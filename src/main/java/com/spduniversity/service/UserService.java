package com.spduniversity.service;

import com.spduniversity.model.User;

public interface UserService {
    public User getUserById(Integer id);
}
