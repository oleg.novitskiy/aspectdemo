package com.spduniversity;

import com.spduniversity.service.BankService;
import com.spduniversity.service.UserService;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) throws Exception {
        AbstractApplicationContext  context = new ClassPathXmlApplicationContext("application-context.xml");

        BankService bankService = (BankService) context.getBean("bankService");
        bankService.transferMoney(1000L);
        System.out.println();
        long sum = bankService.borrowMoney(200L);
        System.out.println("sum : " + sum);

        UserService userService = (UserService) context.getBean("userService");
        System.out.println(userService.getUserById(1));
    }

}
